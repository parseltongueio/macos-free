#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <mach/mach.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <math.h>
#include <inttypes.h>

const char * const VERSION = "0.4";

typedef union mem_stats_u {
    struct mem_stats_s {
        int64_t total,      used,      free,
                wire,       in_active, spec,
                comp,       purge,
                swap_total, swap_used, swap_free;
    } mem_stats_s;
    int64_t mem_stats_a[11];
} mem_stats_t;

typedef union magic_nums_u {
    struct magic_nums_s {
        int64_t b, k, m, g, t, p;
    } magic_nums_s;
    int64_t magic_nums_a[5];
} magic_nums_t;

void usage(){
    fprintf(stderr,"Usage:\n");
    fprintf(stderr," free [option]\n\n");
    fprintf(stderr,"Options:\n");
    fprintf(stderr," -b, --bytes\t show output in bytes\n");
    fprintf(stderr,"     --kilo\t show output in kilobytes\n");
    fprintf(stderr,"     --mega\t show output in megabytes\n");
    fprintf(stderr,"     --giga\t show output in gigabytes\n");
    fprintf(stderr,"     --tera\t show output in terabytes\n");
    fprintf(stderr,"     --peta\t show output in petabytes\n");
    fprintf(stderr," -k, --kibi\t show output in kibibytes\n");
    fprintf(stderr," -m, --mebi\t show output in mebibytes\n");        
    fprintf(stderr," -g, --gibi\t show output in gibibytes\n");        
    fprintf(stderr," -t, --tebi\t show output in tebibytes\n");        
    fprintf(stderr," -p, --peti\t show output in pebibytes\n");
    fprintf(stderr,"     --si\t use powers of 1000 not 1024\n");
    fprintf(stderr," -h  --human\t show human-readable output\n");
    fprintf(stderr,"\n");            
    fprintf(stderr," -H, --help\t display help and exit\n");
    fprintf(stderr," -V, --version\t display version and exit\n");
}

void version(){
    fprintf(stderr, "free for macOS %s\n",VERSION);
}

magic_nums_t get_magic_numbers(int si){
    int64_t magic_multiplier = 1024;
    if (si == 1){
        magic_multiplier = 1000;
    }
    magic_nums_t nums = {
        .magic_nums_s = {
            (int64_t) 1,
            (int64_t) magic_multiplier,
            (int64_t) pow(magic_multiplier, 2),
            (int64_t) pow(magic_multiplier, 3),
            (int64_t) pow(magic_multiplier, 4),
            (int64_t) pow(magic_multiplier, 5),
        }
    };
    return nums;
}

int64_t get_magic_number(int unit, magic_nums_t * magic_numbers){
    switch(unit){
    case 'k':
        return magic_numbers->magic_nums_s.k;
    case 'm':
        return magic_numbers->magic_nums_s.m;
    case 'g':
        return magic_numbers->magic_nums_s.g;
    case 't':
        return magic_numbers->magic_nums_s.t;
    case 'p':
        return magic_numbers->magic_nums_s.p;
    }
    return magic_numbers->magic_nums_s.b;
}

void get_stats(vm_statistics64_t stat)
{
    mach_msg_type_number_t count = HOST_VM_INFO64_COUNT;
    kern_return_t ret;
    if ((ret = host_statistics64(mach_host_self(), HOST_VM_INFO64, (host_info64_t)stat, &count) != KERN_SUCCESS)) {
        fprintf(stderr, "failed to get statistics. error %d\n", ret);
        exit(EXIT_FAILURE);
    }
}

int64_t get_physical_memory(){
    int mib[2] = {CTL_HW, HW_MEMSIZE};
    size_t length = sizeof(int64_t);
    int64_t physical_memory;
    sysctl(mib, 2, &physical_memory, &length, NULL, 0);
    return physical_memory;
}

void get_xsw(struct xsw_usage *xsw){
    int mib[2] = {CTL_VM, VM_SWAPUSAGE};
    size_t xswlen = sizeof(*xsw);
    sysctl(mib, 2, xsw, &xswlen, NULL, 0);
}

const char* get_human(int64_t x, int si, magic_nums_t * magic_numbers){
    char *s;
    if( x < magic_numbers->magic_nums_s.k ){
        asprintf(&s, "%0.1LfKi", (long double) x / magic_numbers->magic_nums_s.k);
    }else if( x < magic_numbers->magic_nums_s.m ){
        asprintf(&s, "%0.1LfKi", (long double) x / magic_numbers->magic_nums_s.k);
    }else if( x < magic_numbers->magic_nums_s.g ){
        asprintf(&s, "%lluMi", x / magic_numbers->magic_nums_s.m);
    }else if( x  < magic_numbers->magic_nums_s.t ){
        asprintf(&s, "%0.1LfGi", (long double) x / magic_numbers->magic_nums_s.g);
    }else if( x < magic_numbers->magic_nums_s.p ){
        asprintf(&s, "%0.1LfTi", (long double) x / magic_numbers->magic_nums_s.t);
    }else if( x > magic_numbers->magic_nums_s.p ){
        asprintf(&s, "%lluPi", x / magic_numbers->magic_nums_s.p);
    }else{
        asprintf(&s, "%lluBytes", x / magic_numbers->magic_nums_s.b);
    }
    if(si){
        s[strlen(s)-1] = '\0';
    }
    return(s);
}

void print_free(char unit, int si){
    vm_statistics64_data_t vm_stat; get_stats(&vm_stat);
    struct xsw_usage xsw;           get_xsw(&xsw); // swap struct
    mem_stats_t stats = { 
        .mem_stats_s = {
            (int64_t) get_physical_memory(),
            (vm_stat.wire_count + vm_stat.throttled_count + vm_stat.active_count) * vm_kernel_page_size,
            (vm_stat.free_count - vm_stat.speculative_count) * vm_kernel_page_size,
            (vm_stat.wire_count + vm_stat.throttled_count) * vm_kernel_page_size,
            (vm_stat.active_count + vm_stat.inactive_count) * vm_kernel_page_size,
            vm_stat.speculative_count * vm_kernel_page_size,
            vm_stat.compressor_page_count * vm_kernel_page_size,
            vm_stat.purgeable_count * vm_kernel_page_size,
            (int64_t) xsw.xsu_total,
            (int64_t) xsw.xsu_used ,
            (int64_t) (xsw.xsu_total - xsw.xsu_used)
        }
    };

    magic_nums_t magic_numbers = get_magic_numbers(si);
    int64_t magic_number = get_magic_number(unit, &magic_numbers);
    int col_width = 8; // longest title + 1 (inac/ac)
    for(size_t i = 0; i < sizeof(union mem_stats_u)/sizeof(int64_t); i++){
      const int neg = stats.mem_stats_a[i] < 0 ? 1 : 0; // this should never be negative; here for safety
      int num_digits = log10(llabs(stats.mem_stats_a[i])) + 1 + neg;
      if ( col_width <= num_digits )
          col_width = num_digits + 1;
    };
    printf("      %-*s%-*s%-*s%-*s%-*s%-*s%-*s%-*s\n",
	   col_width, "total",
	   col_width, "used",
	   col_width, "free",
	   col_width, "wire",
	   col_width, "inac/ac",
	   col_width, "spec",
	   col_width, "comp",
	   col_width, "purge"
    );
    if ( unit == 'h' ){
        printf("Mem:  %-*s%-*s%-*s%-*s%-*s%-*s%-*s%-*s\n",
    	   col_width, get_human(stats.mem_stats_s.total, si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.used,  si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.free,  si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.wire,  si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.in_active, si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.spec,  si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.comp,  si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.purge, si, &magic_numbers)
        );
        printf("Swap: %-*s%-*s%-*s\n",
    	   col_width, get_human(stats.mem_stats_s.swap_total, si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.swap_used,  si, &magic_numbers),
    	   col_width, get_human(stats.mem_stats_s.swap_free,  si, &magic_numbers)
        );
    } else {
        printf("Mem:  %-*" PRId64 "%-*" PRId64 "%-*" PRId64 "%-*" PRId64 "%-*" PRId64 "%-*" PRId64 "%-*" PRId64 "%-*" PRId64 "\n",
    	   col_width, stats.mem_stats_s.total / magic_number,
    	   col_width, stats.mem_stats_s.used  / magic_number,
    	   col_width, stats.mem_stats_s.free  / magic_number,
    	   col_width, stats.mem_stats_s.wire  / magic_number,
    	   col_width, stats.mem_stats_s.in_active / magic_number,
    	   col_width, stats.mem_stats_s.spec  / magic_number,
    	   col_width, stats.mem_stats_s.comp  / magic_number,
    	   col_width, stats.mem_stats_s.purge / magic_number
        );
        printf("Swap: %-*" PRId64 "%-*" PRId64 "%-*" PRId64 "\n",
    	   col_width, stats.mem_stats_s.swap_total / magic_number,
    	   col_width, stats.mem_stats_s.swap_used  / magic_number,
    	   col_width, stats.mem_stats_s.swap_free  / magic_number
        );
    }
}
  
int main(int argc, char * const *argv) {
    int c;
    int bflag = 0;
    int kflag = 0;
    int mflag = 0;
    int gflag = 0;
    int tflag = 0;
    int pflag = 0;
    int hflag = 0;
    static int si = 0;
    static struct option longopts[] = {
        { "bytes",   no_argument, NULL, 'b' },
        { "kibi",    no_argument, NULL, 'k' },
        { "mebi",    no_argument, NULL, 'm' },
        { "gibi",    no_argument, NULL, 'g' },
        { "tebi",    no_argument, NULL, 't' },
        { "pebi",    no_argument, NULL, 'p' },
        { "help",    no_argument, NULL, 'H' },
        { "human",   no_argument, NULL, 'h' },
        { "si",      no_argument, &si,   1  },
        { "kilo",    no_argument, &si,   1  },
        { "mega",    no_argument, &si,   1  },
        { "giga",    no_argument, &si,   1  },
        { "tera",    no_argument, &si,   1  },
        { "peta",    no_argument, &si,   1  },
	{ "version", no_argument, NULL, 'V' }
    };

    int option_index = 0;
    int enabled_flags = 0;    
    while ((c = getopt_long(argc, argv, "bkmgthpHV :", longopts, &option_index)) != -1) {
        switch(c){
        case 0:
            if (strcmp(longopts[option_index].name,"kilo") == 0){
                kflag = 1;
                enabled_flags += 1;
            } else if (strcmp(longopts[option_index].name,"mega") == 0){
                mflag = 1;
                enabled_flags += 1;
            } else if (strcmp(longopts[option_index].name,"giga") == 0){
                gflag = 1;
                enabled_flags += 1;
            } else if (strcmp(longopts[option_index].name,"tera") == 0){
                tflag = 1;
                enabled_flags += 1;
            } else if (strcmp(longopts[option_index].name,"peta") == 0){
                pflag = 1;
                enabled_flags += 1;
            }
            break;
	case 'b':
	    bflag = 1;
 	    enabled_flags += 1;
            break;
	case 'k':
	    kflag = 1;
	    enabled_flags += 1;
            break;
	case 'm':
	    mflag = 1;
	    enabled_flags += 1;
            break;
	case 'g':
	    gflag = 1;
	    enabled_flags += 1;
            break;
	case 't':
	    tflag = 1;
	    enabled_flags += 1;
            break;
	case 'p':
	    pflag = 1;
	    enabled_flags += 1;
            break;
	case 'h':
	    hflag = 1;
	    enabled_flags += 1;
            break;
	case 'H':
	    usage();
            exit(0);
	case 'V':
	    version();
            exit(0);
        default:
            exit(1);
        }
    }
    
    if (enabled_flags > 1){
        fprintf(stderr,"error: Too many options\n");
        usage();
        exit(1);
    }
    if (bflag)
      print_free('b',si);
    else if (kflag)
      print_free('k',si);
    else if (mflag)
      print_free('m',si);
    else if (gflag)
      print_free('g',si);
    else if (tflag)
      print_free('t',si);
    else if (pflag)
      print_free('p',si);
    else if (hflag)
      print_free('h',si);
    else
      print_free('b',si);
    return 0;
}
